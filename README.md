# Description du projet
Vin sur vingt est une application web mobile qui a pour but la sauvegarde de ses
vins favoris ainsi que de recettes associées à ceux-ci.

Projet INSA 4A - Web Avancée - Par [PIROIRD Romain](https://gitlab.com/piroird),
[ARLAUD Hortense](https://gitlab.com/HortenseA),
[COPPYN Florian](https://gitlab.com/florian.coppyn)


# Configuration du projet
## Configuration du serveur
La configuration du serveur se fait dans le fichier `.env`.
- PORT : le port utilisé par le serveur
- URL : le suffixe ajouté à l'url du serveur, laissé vide si non souhaité
- SECRET : le mot secret utilisé pour la sauvegarde des mots de passes
- TIME_COOKIES : la durée de sauvegarde des cookies (en ms)

## Configuration du client
La configuration du client se fait dans les fichier `environement.ts` et
`environment.prod.ts` si génération en mode production. Le fichier doit contenir
l'url de l'API dans la clé "apiURL".


# Lancement du programme
## Démarage du serveur
1. Se positionner dans le dossier Server
2. (Première fois) `npm install` pour installer les dépendances
3. (Première fois) `npm run init` pour installer la BDD
4. Lancement du serveur :
  - `node main` pour un lancement en production
  - `npm run dev` pour un lancement en développement

## Démarage du client
1. Se positionner dans le dossier Client
2. (Première fois) `npm install` pour installer les dépendances
3. Lancement du serveur :
  - `node main` pour un lancement en production
  - `ng serve -o` pour un lancement en développement
