DROP TABLE IF EXISTS FAVORIS;
DROP TABLE IF EXISTS ACCORDER;
DROP TABLE IF EXISTS AVIS;
DROP TABLE IF EXISTS COMMENTAIRE;
DROP TABLE IF EXISTS RECETTE;
DROP TABLE IF EXISTS VIN;
DROP TABLE IF EXISTS PERSONNE;
DROP TABLE IF EXISTS COULEUR_VIN;

create table COULEUR_VIN(
    label varchar(5) primary key
);
insert into COULEUR_VIN values ("Blanc"), ("Rosé"), ("Rouge");

create table PERSONNE(
    login varchar(20) primary key,
    pwd varchar(64)
);

create table RECETTE(
    idR integer primary key autoincrement,
    nom varchar(50),
    temps varchar(10),
    preparations text,
    img varchar(32)
);

create table VIN(
    idV integer primary key autoincrement,
    titre varchar(50),
    descr text,
    provenance varchar(50),
    domaine varchar(50),
    cépage varchar(50),
    couleur varchar(5) references COULEUR_VIN(label),
    petillant boolean,
    annee integer(4),
    img varchar(32)
);

create table COMMENTAIRE(
    login varchar(20) references PERSONNE(login),
    idV integer references VIN(idV),
    message text,
    primary key(login, idV)
);

create table AVIS(
    login varchar(20) references PERSONNE(login),
    idV integer references VIN(idV),
    note integer,
    primary key(login, idV)
);

create table ACCORDER(
    idR integer references RECETTE(idR),
    couleur integer references COULEUR_VIN(label),
    conseil text,
    primary key(idR, couleur)
);

create table FAVORIS(
    login varchar(20) references PERSONNE(login),
    idV integer references VIN(idV),
    primary key(login, idV)
);


insert into VIN(titre, descr, provenance, domaine, cépage, couleur, petillant, annee, img) values
    ("Cuvée Calcite", "Nesciunt sed repellendus tempora. Ad distinctio enim amet voluptatem. Nulla assumenda vero repellendus. Et et tenetur animi enim in excepturi consequuntur ut. Voluptatem dolor laboriosam eos a aut quis vitae et.",
      "Quincy", "Trotereau", "Sovignot blanc", "Blanc", 0, 2018, "76548.jpg"),
    ("Gevrey-Chambertin", "Nesciunt sed repellendus tempora. Ad distinctio enim amet voluptatem. Nulla assumenda vero repellendus. Et et tenetur animi enim in excepturi consequuntur ut. Voluptatem dolor laboriosam eos a aut quis vitae et.",
      "Bourgogne", "Arlaud", "Pinot Noir", "Rouge", 0, 2017, NULL);

insert into RECETTE(nom, temps, preparations) values
	("Choucroute", "2h30", "Laver la choucroute. Faites la cuire avec des oignons, du lard fumé, des saucisses fumées de Montbéliard pendant 2h à feu doux. Cuire les patates et les éplucher. Ajouter les avant la fin de la cuisson."),
	("Tournedos aux légumes vapeur", "40min", "Préparer les légumes à la vapeur. Faites saisir le tournedos sur les deux côtés et arroser de beurre régulièrement. Servez.");

insert into ACCORDER(idR, couleur) values
	(1, "Blanc"),
	(2, "Rouge");

insert into FAVORIS(login, idV) values ("booba", 1);

insert into PERSONNE values
    ('kaaris','uERUhhajLBm/+9A62DmumneZ/xMbz+NX6AWdcadhwRY='),
    ('booba','uERUhhajLBm/+9A62DmumneZ/xMbz+NX6AWdcadhwRY=');
-- pwd : totomdr
