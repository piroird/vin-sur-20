const sqlite3 = require('sqlite3');

let _db;

function initDB(){
  return new Promise((resolve, reject) => {
    _db = new sqlite3.Database('./database/datas.db', sqlite3.OPEN_READWRITE, (err) => {
      if (err) {
        console.error(err.message);
        reject();
      }
      else{
        console.log('Connected to database.');
        resolve();
      }
    });
  });
}

function getDB(){
  return _db;
}

function dbAll(sql, param=[]){
  return new Promise( (resolve, reject) =>{
    _db.all(sql, param, (err, rows) => {
      if(err){
        console.log("dbAll", err);
        reject(err);
      }
      else{
        resolve(rows);
      }
    });
  });
}

function dbGet(sql, param=[]){
  return new Promise( (resolve, reject) =>{
    _db.get(sql, param, (err, row) => {
      if(err){
        console.log("dbGet", err);
        reject(err);
      }
      else{
        resolve(row);
      }
    });
  });
}

function dbRun(sql, param=[]){
  return new Promise( (resolve, reject) =>{
    _db.run(sql, param, function(err){
      if(err){
        console.log("dbRun", err);
        reject(err);
      }
      else{
        resolve(this);
      }
    });
  });
}

module.exports = {
    initDB,
    getDB,
    dbAll,
    dbGet,
    dbRun
};
