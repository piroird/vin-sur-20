require('dotenv').config();
const express = require('express');
var cookieParser = require('cookie-parser')

const indexRouter = require('./routes/index.js');
const libDB = require("./database/db");

const PORT = process.env.PORT;
const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cookieParser());
app.use(process.env.URL, indexRouter);

libDB.initDB().then((res)=>{
  app.listen(PORT, ()=>{
    console.log("Serveur listening at "+PORT);
  });
});
