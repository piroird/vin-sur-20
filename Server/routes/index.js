var express = require('express');
var router = express.Router();

let multer = require('multer');
let upload = multer({ dest: 'uploads/' });

const WineCtl = require("../controllers/WineCtl.js");
const UserCtl = require("../controllers/UserCtl.js");
const RecipeCtl = require("../controllers/RecipeCtl.js");

// Routes des vins
router.get('/allWines', WineCtl.getAllWine);
router.get('/getWine', WineCtl.getWine);
router.get('/getFavWine', WineCtl.getFavWine);
router.post('/favWine', WineCtl.favWine);
router.post('/newWine', upload.single('file'), WineCtl.newWine);

// Routes recettes
router.get('/allRecipes', RecipeCtl.getAllRecipe);
router.get('/getRecipe', RecipeCtl.getRecipe);
router.get('/winesMatchedRecipe', RecipeCtl.winesMatchedRecipe);
router.get('/recipesMatchedWine', RecipeCtl.recipesMatchedWine);
router.post('/newRecipe', upload.single('file'), RecipeCtl.newRecipe);

// Routes utilisateur
router.post('/register', UserCtl.register);
router.post('/login', UserCtl.login);
router.post('/logout', UserCtl.logout);

// Route d'envoi des images
router.get('/imgs', (req, res)=>{
  res.sendFile(req.query.name, {root: 'uploads/'});
});

module.exports = router;
