const libDB = require("../database/db");
const User = require('../models/User.js');
const libCrypto = require('./libCrypto');

function register(req, res){
  if(req.body.login == null || req.body.pwd == null){
    res.status(500).send();
    return;
  }

  let user = new User(
    req.body.login,
    libCrypto.getInstance().hash_pwd(req.body.pwd)
  );

  let sql = "insert into PERSONNE values (?, ?);";
  libDB.dbRun(sql, [user.login, user.pwd_hash]).then(()=>{
    let authToken = libCrypto.getInstance().generateAuthToken(user);
    libCrypto.setCookieAuth(res, authToken);
    res.send(true);
  }).catch(()=>{
    res.status(500).send();
  });
}

function login(req, res){
  if(req.body.login == null || req.body.pwd == null){
    res.status(500).send();
    return;
  }

  let sql = "select * from PERSONNE where login = ?;";
  libDB.dbGet(sql, req.body.login).then(row => {
    let user = User.parseFromDB(row);

    if(libCrypto.getInstance().test_pwd(user, req.body.pwd)){
      let authToken = libCrypto.getInstance().generateAuthToken(user);
      libCrypto.setCookieAuth(res, authToken);
      res.send(true);
    }
    else
      res.status(400).send();
  }).catch(err => {
    res.status(400).send();
  });
}

function logout(req, res){
  let token = libCrypto.getCookieAuth(req);
  if(token != ""){
    libCrypto.getInstance().removeAuthToken(token);
    libCrypto.removeCookieAuth(res);
    res.send(true);
  }
  else{
    res.status(401).send();
  }
}

module.exports = {
    register,
    login,
    logout
};
