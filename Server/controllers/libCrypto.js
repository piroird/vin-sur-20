const crypto = require('crypto');

class PrivateSingleton{
  constructor(){
    this.authTokens = {};
  }

  hash_pwd(pwd){
    return crypto.createHmac('sha256', process.env.SECRET).update(pwd).digest('base64');
  }

  test_pwd(user, pwd){
    return (this.hash_pwd(pwd) == user.pwd_hash);
  }

  generateAuthToken(user){
    let token = crypto.randomBytes(30).toString('hex');
    this.authTokens[token] = user;
    return token;
  }

  removeAuthToken(token){
    delete this.authTokens[token];
  }

  getUser(token){
    if(token in this.authTokens)
      return this.authTokens[token];
    return null;
  }
}

class libCrypto{
  constructor(){
    throw new Error("Use getInstance");
  }

  static getInstance(){
    if(!libCrypto.singleton)
      libCrypto.singleton = new PrivateSingleton();
    return libCrypto.singleton;
  }

  static setCookieAuth(res, token){
    res.cookie('AuthToken', token, { maxAge: process.env.TIME_COOKIES, sameSite: true });
  }

  static removeCookieAuth(res){
    res.cookie("AuthToken", "", { maxAge: 10 });
  }

  static getCookieAuth(req){
    if("AuthToken" in req.cookies)
      return req.cookies.AuthToken;
    return "";
  }
}

module.exports = libCrypto;
