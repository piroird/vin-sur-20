const libDB = require("../database/db");
const Recipe = require('../models/Recipe.js');
const Wine = require('../models/Wine.js');
const libCrypto = require('./libCrypto');

function getAllRecipe(req, res){
    let sql = "select * from RECETTE;";

    libDB.dbAll(sql).then(rows => {
      let recipes = Recipe.parseListFromDB(rows);
      res.send(recipes);
    }).catch(err => {
      res.status(500).send()
    });
  }

function getRecipe(req, res){
  let sql = "select * from RECETTE where idR = ?;";

  libDB.dbGet(sql, req.query.id).then(row => {
    let recipe = Recipe.parseFromDB(row);
    res.send(recipe);
  }).catch(err => {
    res.status(500).send()
  });
}

function winesMatchedRecipe(req, res){
  let sql = "select V.* from VIN V NATURAL JOIN ACCORDER where idR = ?;";

  libDB.dbAll(sql, req.query.idR).then(rows => {
    let wines = Wine.parseListFromDB(rows);
    res.send(wines);
  }).catch(err => {
    res.status(500).send()
  });
}

function recipesMatchedWine(req, res){
  let sql = "select R.* from RECETTE R NATURAL JOIN ACCORDER where couleur = ?;";

  libDB.dbAll(sql, req.query.color).then(rows => {
    let recipes = Recipe.parseListFromDB(rows);
    res.send(recipes);
  }).catch(err => {
    res.status(500).send()
  });
}

function verifyNewRecipe(body){
  return !(body.nom == null || body.nom.length > 50 || body.preparation == null
      || body.temps == null || body.temps.length > 10 || body.accords == null)
}

function newRecipe(req, res){
  let user = libCrypto.getInstance().getUser(libCrypto.getCookieAuth(req));
  if(user == null){
    res.status(401).send();
    return;
  }

  if(!verifyNewRecipe(req.body)){
    res.status(500).send();
    return;
  }

  let sql = `insert into RECETTE(nom, temps, preparations, img) values (?, ?, ?, ?);`;

  let params = [
    req.body.nom,
    req.body.temps,
    req.body.preparation,
    req.file ? req.file.filename : null
  ];

  libDB.dbRun(sql, params).then((ok)=>{
    let idR = ok.lastID;
    let colors = req.body.accords.split(",");

    let end = colors.map((c) => `(${idR}, ?)`).join(',');
    sql = "insert into ACCORDER(idR, couleur) values "+end;

    libDB.dbRun(sql, colors).then(()=>{
      res.send(true);
    }).catch(err=>{
      res.status(500).send();
    });
  }).catch(err=>{
    console.log(err);
    res.status(500).send();
  });
}

module.exports = {
    getAllRecipe,
    newRecipe,
    getRecipe,
    winesMatchedRecipe,
    recipesMatchedWine
};
