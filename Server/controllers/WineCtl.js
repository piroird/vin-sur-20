const libDB = require("../database/db");
const Wine = require('../models/Wine.js');
const libCrypto = require('./libCrypto');

function getAllWine(req, res){
  let sql = "select * from VIN;";

  libDB.dbAll(sql).then(rows => {
    let wines = Wine.parseListFromDB(rows);
    res.send(wines);
  }).catch(err => {
    res.status(500).send()
  });
}

function getWine(req, res){
  let user = libCrypto.getInstance().getUser(libCrypto.getCookieAuth(req));
  let sql = "select * from VIN where idV = ?;";

  libDB.dbGet(sql, req.query.id).then(row => {
    let wine = Wine.parseFromDB(row);
    if(user != null){
      sql = "select * from FAVORIS where idV = ? and login = ?;";
      libDB.dbGet(sql, [req.query.id, user.login]).then(ok => {
        if(ok != null)
          wine.fav = true;
        res.send(wine);
      }).catch(err => {
        res.status(500).send()
      });
    }
    else res.send(wine);
  }).catch(err => {
    res.status(500).send()
  });
}

function favWine(req, res){
  let user = libCrypto.getInstance().getUser(libCrypto.getCookieAuth(req));
  if(user == null){
    res.status(401).send();
    return;
  }

  if(req.body.idV == null || req.body.value == null){
    res.status(500).send();
    return;
  }

  let sql = req.body.value ?
    "insert into FAVORIS values (?, ?);" :
    "delete from FAVORIS where (login=? and idV=?);";

  libDB.dbRun(sql, [user.login, req.body.idV]).then(()=>{
    res.send(true);
  }).catch(()=>{
    res.status(500).send();
  });
}

function verifyNewWine(body){
  const MIN_YEAR = 1900;
  const CURRENT_YEAR = new Date().getFullYear();
  const COLORS = ["Blanc", "Rosé", "Rouge"];
  const BOOLEANS = ["true", "false"];

  if(body.titre == null || body.titre.length > 50 || body.descr == null
      || body.provenance == null  || body.provenance.length > 50
      || body.domaine == null     || body.domaine.length > 50
      || body.cépage == null      || body.cépage.length > 50
      || body.couleur == null     || !COLORS.includes(body.couleur)
      || !BOOLEANS.includes(body.petillant)){
    return false;
  }

  let year = Number(body.annee);
  return (MIN_YEAR <= year && year <= CURRENT_YEAR);
}

function newWine(req, res){
  let user = libCrypto.getInstance().getUser(libCrypto.getCookieAuth(req));
  if(user == null){
    res.status(401).send();
    return;
  }

  if(!verifyNewWine(req.body)){
    res.status(500).send();
    return;
  }

  let sql = `insert into VIN(titre, descr, provenance, domaine, cépage, couleur,
      petillant, annee, img) values (?, ?, ?, ?, ?, ?, ?, ?, ?);`;

  let params = [
    req.body.titre,
    req.body.descr,
    req.body.provenance,
    req.body.domaine,
    req.body.cépage,
    req.body.couleur,
    req.body.petillant ? "1" : "0",
    req.body.annee,
    req.file ? req.file.filename : null
  ];

  libDB.dbRun(sql, params).then(()=>{
    res.send(true);
  }).catch(()=>{
    res.status(500).send();
  });
}

function getFavWine(req, res){
  let user = libCrypto.getInstance().getUser(libCrypto.getCookieAuth(req));
  if(user == null){
    res.status(401).send();
    return;
  }

  let sql = "select * from FAVORIS NATURAL JOIN VIN where login=?;";

  libDB.dbAll(sql, [user.login]).then(rows => {
    let wines = Wine.parseListFromDB(rows);
    wines = wines.map(wine =>{
      wine["fav"]= true;
      return wine});
    res.send(wines);
  }).catch(err => {
    res.status(500).send()
  });
}

module.exports = {
    getAllWine,
    getWine,
    favWine,
    newWine,
    getFavWine
};
