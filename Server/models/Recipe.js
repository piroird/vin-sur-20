class Recipe{
    constructor(id, name, time, preparation, img){
      this.id = id;
      this.name = name;
      this.time = time;
      this.preparation = preparation;
      this.img = img;
      // this.note = -1;
      // this.comments = []
    }
  
    static parseFromDB(row){
  
      return new Recipe(
        row.idR,
        row.nom,
        row.temps,
        row.preparations,
        row.img
      );
    }
  
    static parseListFromDB(rows){
      let res = [];
      rows.forEach(row => {
        res.push(Recipe.parseFromDB(row));
      });
      return res;
    }
  }
  
  module.exports = Recipe;