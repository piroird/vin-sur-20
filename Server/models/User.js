class User{
  constructor(login, pwd_hash){
    this.login = login;
    this.pwd_hash = pwd_hash;
  }

  static parseFromDB(row){
    return new User(row.login, row.pwd)
  }
}

module.exports = User;
