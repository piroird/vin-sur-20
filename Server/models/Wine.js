class Wine{
  constructor(id, title, description, color, bubble, grape_variety, origin, domain, year, img){
    this.id = id;
    this.title = title;
    this.description = description;
    this.color = color;
    this.bubble = bubble;
    this.grape_variety = grape_variety;
    this.origin = origin;
    this.domain = domain;
    this.year = year;
    this.img = img;
    this.fav = null;
    // this.note = -1;
    // this.comments = []
  }

  static parseFromDB(row){

    return new Wine(
      row.idV,
      row.titre,
      row.descr,
      row.couleur,
      row.petillant,
      row.cépage,
      row.provenance,
      row.domaine,
      row.annee,
      row.img
    );
  }

  static parseListFromDB(rows){
    let res = [];
    rows.forEach(row => {
      res.push(Wine.parseFromDB(row));
    });
    return res;
  }
}

module.exports = Wine;
