import { Component, OnInit } from '@angular/core';
import { RecipeService } from "../recipe.service";
import { Recipe } from "../recipe";

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.scss']
})
export class RecipeListComponent implements OnInit {
  recipeList: any[] = [];
  search: string;
  showPreparation: boolean = false;

  constructor(private recipeService:RecipeService) { }

  ngOnInit(): void {
    this.recipeService.getAllRecipes().subscribe(recipes => this.recipeList = recipes);
  }

  getLink(recipe: Recipe): string{
    return `/recette/${recipe.id}`;
  }
}
