import { Component, OnInit, Output } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { environment as env } from "../../environments/environment";
import { WineService } from "../wine.service";
import { UserService } from "../user.service";
import { Wine } from "../wine";
import { Recipe} from "../recipe";

@Component({
  selector: 'app-wine-page',
  templateUrl: './wine-page.component.html',
  styleUrls: ['./wine-page.component.scss']
})
export class WinePageComponent implements OnInit {
  id: number;
  color: String;
  wine: Wine;
  img_url: string;
  recipes : Recipe[];

  constructor(private route: ActivatedRoute, private wineService: WineService, private userService: UserService) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.id = parseInt(params['id']);
      this.wineService.getWine(this.id).subscribe(wine => {
        this.wine = wine;
        this.color = wine.color;
        this.img_url = `${env.apiURL}/imgs?name=${wine.img}`;
      this.wineService.getMatchedRecipe(this.color).subscribe(recipes => this.recipes = recipes);
      });
    });
  }

  addFav(){
    this.wineService.favWine(this.id, !this.wine.fav).subscribe(ok=>{
      this.wine.fav = !this.wine.fav;
    });
  }

  getLink(recipe: Recipe): string{
    return `/recette/${recipe.id}`;
  }
}
