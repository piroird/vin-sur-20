import { Component, Input, OnInit } from '@angular/core';
import { environment as env } from "../../environments/environment";

@Component({
  selector: 'listElem',
  templateUrl: './list-elem.component.html',
  styleUrls: ['./list-elem.component.scss']
})
export class ListElemComponent implements OnInit {
  @Input() title: string;
  @Input() description: string;
  @Input() icon: string;
  @Input() iconTxt: string;
  @Input() img: string;
  @Input() link: string;

  img_url: string;

  constructor() { }

  ngOnInit() {
    this.img_url = `${env.apiURL}/imgs?name=${this.img}`;
  }
}
