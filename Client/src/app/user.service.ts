import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment as env} from '../environments/environment';
import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  connected = false;
  private source = new BehaviorSubject("");
  updateObservable = this.source.asObservable();

  constructor(private http: HttpClient) { }

  login(username, pwd): Observable<boolean>{
    return this.http.post<boolean>(env.apiURL+"login", {
      "login": username,
      "pwd": pwd
    });
  }

  register(username, pwd): Observable<boolean>{
    return this.http.post<boolean>(env.apiURL+"register", {
      "login": username,
      "pwd": pwd
    });
  }

  logout(): Observable<boolean>{
    localStorage.removeItem("AuthToken");
    document.cookie = 'AuthToken=; Max-Age=0;';
    this.connected = false;
    return this.http.post<boolean>(env.apiURL+"logout", null);
  }

  saveAuth(){
    let list = document.cookie.split("; ");
    for(let cookie of list){
      let info = cookie.split("=");
      if(info[0] == "AuthToken"){
        localStorage.setItem("AuthToken", info[1]);
        this.connected = true;
        return true;
      }
    }
    throw new Error("no auth cookie");
  }

  authIfICan(){
    let list = document.cookie.split("; ");
    for(let cookie of list){
      let info = cookie.split("=");
      if(info[0] == "AuthToken"){
        localStorage.setItem("AuthToken", info[1]);
        this.connected = true;
        return true;
      }
    }
    return false;
  }

  sendUpdate(){
    this.source.next("");
  }
}
