import { Component, OnInit } from '@angular/core';
import { SelectItem, MessageService } from 'primeng/api';
import { environment as env } from "../../environments/environment";
import { WineService } from "../wine.service";
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-wine',
  templateUrl: './add-wine.component.html',
  styleUrls: ['./add-wine.component.scss']
})
export class AddWineComponent implements OnInit{
  colors: any[];
  origin: SelectItem[];
  actual_year: number = new Date().getFullYear();

  file_img: File;
  name: string;
  selectedColor: string;
  petillant: boolean = false;
  selectedOrigin: string;
  domaine: string;
  description: string;
  grape_variety: string;
  year: number = 2010;

  constructor(private router: Router, private messageService: MessageService, private wineService: WineService) {}

  ngOnInit() {
    this.origin = [
      {label: "Alsace", value: "Alsace"},
      {label: "Armagnac/Cognac", value: "Armagnac/Cognac"},
      {label: "Beaujolais", value: "Beaujolais"},
      {label: "Bordeaux", value: "Bordeaux"},
      {label: "Bourgogne", value: "Bourgogne"},
      {label: "Champagne", value: "Bourgogne"},
      {label: "Corse", value: "Bourgogne"},
      {label: "Jura", value: "Bourgogne"},
      {label: "Languedoc", value: "Bourgogne"},
      {label: "Lorraine", value: "Bourgogne"},
      {label: "Poitou-Charentes", value: "Bourgogne"},
      {label: "Provence", value: "Bourgogne"},
      {label: "Roussillon", value: "Bourgogne"},
      {label: "Savoie", value: "Bourgogne"},
      {label: "Sud-Ouest", value: "Bourgogne"},
      {label: "Vallée de la Loire", value: "Bourgogne"},
      {label: "Vallée du Rhône", value: "Bourgogne"}
    ];
    this.colors = [
      {name: 'Blanc', code: 'Blanc'},
      {name: 'Rouge', code: 'Rouge'},
      {name: 'Rosé', code: 'Rosé'},
    ]
  }

  openInput(){
    document.getElementById("inputFile").click();
  }

  onFileChange(event){
    let f: File = event.target.files[0];
    if (f.size > 1000000){
      this.messageService.add({severity:'error', summary:'Fichier trop lourd', detail:"Impossible d'ajouter une image de +1Mo."});
      event.target.value = "";
      this.file_img = null;
    }
    else{
      this.file_img = f;
      let span = document.querySelector("div#upload span") as HTMLElement;
      span.innerText = f.name;
    }
  }

  submitForm(){
    let data = new FormData();
    data.append("file", this.file_img);
    data.append("titre", this.name);
    data.append("descr", this.description);
    data.append("provenance", this.selectedOrigin);
    data.append("domaine", this.domaine);
    data.append("cépage", this.grape_variety);
    data.append("couleur", this.selectedColor);
    data.append("petillant", `${this.petillant}`);
    data.append("annee", `${this.year}`);

    this.wineService.newWine(data).subscribe(() => {
      this.router.navigate(["/"]);
    });
  }

}
