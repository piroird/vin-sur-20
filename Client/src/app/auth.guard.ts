import { Injectable } from '@angular/core';
import { CanActivate, CanLoad, Route, Router, UrlSegment, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanLoad {
  constructor(private router: Router){ }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    return this.checkToken();
  }

  canLoad(route: Route, segments: UrlSegment[]): boolean {
    return this.checkToken();
  }

  checkToken(): boolean {
    if (localStorage.getItem('AuthToken')) {
      return true;
    }
    else {
      this.router.navigateByUrl('/connexion');
      return false;
    }
  }
}
