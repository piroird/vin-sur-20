import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { environment as env } from "../../environments/environment";
import { RecipeService } from "../recipe.service";
import { Recipe } from "../recipe";
import { Wine} from "../wine";

@Component({
  selector: 'app-recipe-page',
  templateUrl: './recipe-page.component.html',
  styleUrls: ['./recipe-page.component.scss']
})
export class RecipePageComponent implements OnInit {
  id: number;
  recipe: Recipe;
  img_url: string;
  accords: Wine[];

  constructor(private route: ActivatedRoute, private recipeService:RecipeService) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.id = parseInt(params['id']);
      this.recipeService.getRecipe(this.id).subscribe(recipe => {
        this.recipe = recipe;
        this.img_url = `${env.apiURL}/imgs?name=${recipe.img}`;
      this.recipeService.getMatchedWine(this.id).subscribe(accords => this.accords = accords);
      });
    });
  }

  wineDescription(wine: Wine): string{
    return `${wine.origin}, domaine ${wine.domain}, ${wine.year}`;
  }

  getLink(wine: Wine): string{
    return `/vin/${wine.id}`;
  }

}
