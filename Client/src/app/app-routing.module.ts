import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from "./home/home.component";
import { WinePageComponent } from './wine-page/wine-page.component';
import { AddWineComponent } from './add-wine/add-wine.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { FavListComponent } from './fav-list/fav-list.component';
import { RecipeListComponent } from './recipe-list/recipe-list.component';
import { RecipePageComponent } from './recipe-page/recipe-page.component';
import { AddRecipeComponent } from './add-recipe/add-recipe.component';
import { AuthGuard } from './auth.guard';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'vin/:id', component: WinePageComponent },
  { path: 'ajouter-vin', component: AddWineComponent, canActivate: [AuthGuard] },
  { path: 'connexion', component: SignInComponent, data: { login: true } },
  { path: 'deconnexion', component: SignInComponent, data: { logout: true } },
  { path: 'inscription', component: SignInComponent, data: { login: false } },
  { path: 'liste-favoris', component: FavListComponent },
  { path: 'liste-recettes', component: RecipeListComponent },
  { path: 'recette/:id', component: RecipePageComponent },
  { path: 'ajouter-recette', component: AddRecipeComponent, canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
