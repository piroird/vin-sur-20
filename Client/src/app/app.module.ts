import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { PrimengModule } from './primeng.module';
import { MessageService } from 'primeng/api';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ListElemComponent } from './list-elem/list-elem.component';
import { WinePageComponent } from './wine-page/wine-page.component';
import { AddWineComponent } from './add-wine/add-wine.component';
import { FavListComponent } from './fav-list/fav-list.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { HeaderComponent } from './header/header.component';
import { RecipeListComponent } from './recipe-list/recipe-list.component';
import { AddRecipeComponent } from './add-recipe/add-recipe.component';
import { RecipePageComponent } from './recipe-page/recipe-page.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ListElemComponent,
    WinePageComponent,
    AddWineComponent,
    FavListComponent,
    SignInComponent,
    HeaderComponent,
    RecipeListComponent,
    AddRecipeComponent,
    RecipePageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    PrimengModule
  ],
  providers: [MessageService],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
