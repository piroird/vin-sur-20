import { NgModule } from '@angular/core';

import {AccordionModule} from 'primeng/accordion';
import {ButtonModule} from 'primeng/button';
import {SidebarModule} from 'primeng/sidebar';
import {InputTextModule} from 'primeng/inputtext';
import {SliderModule} from 'primeng/slider';
import {TableModule} from 'primeng/table';
import {PasswordModule} from 'primeng/password';
import {ToastModule} from 'primeng/toast';
import {SelectButtonModule} from 'primeng/selectbutton';
import {CheckboxModule} from 'primeng/checkbox';
import {DropdownModule} from 'primeng/dropdown';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {InputNumberModule} from 'primeng/inputnumber';
import {TriStateCheckboxModule} from 'primeng/tristatecheckbox';
import {MultiSelectModule} from 'primeng/multiselect';
import {FileUploadModule} from 'primeng/fileupload';

let modules = [
  AccordionModule,
  ButtonModule,
  SidebarModule,
  InputTextModule,
  SliderModule,
  TableModule,
  PasswordModule,
  ToastModule,
  SelectButtonModule,
  CheckboxModule,
  DropdownModule,
  InputTextareaModule,
  InputNumberModule,
  TriStateCheckboxModule,
  MultiSelectModule,
  FileUploadModule
];

@NgModule({
  imports: modules,
  exports: modules
})
export class PrimengModule { }
