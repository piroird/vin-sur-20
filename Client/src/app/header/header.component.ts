import { Component, OnInit } from '@angular/core';
import { UserService } from "../user.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  title = 'Vin sur Vingt';
  displaySideBar = false;
  links: any[] = []

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.userService.authIfICan();
    this.updateLinks();
    this.userService.updateObservable.subscribe(()=>{
      this.updateLinks();
    });
  }

  updateLinks(){
    if(this.userService.connected) this.links = [
      {url: "/", label: "Liste des vins"},
      {url: "/liste-favoris", label: "Liste des favoris"},
      {url: "/ajouter-vin", label: "Ajouter un vin"},
      {url: "/ajouter-recette", label: "Ajouter une recette"},
      {url: "/deconnexion", label: "Déconnexion"}
    ];
    else this.links = [
      {url: "/", label: "Liste des vins"},
      {url: "/liste-recettes", label: "Liste des recettes"},
      {url: "/connexion", label: "Connexion"},
      {url: "/inscription", label: "Inscription"},
    ];
  }
}
