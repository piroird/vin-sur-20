import { Component, OnInit } from '@angular/core';
import { UserService } from "../user.service";
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {
  login:boolean;
  username: string;
  password: string;

  constructor(private userService: UserService, private route: ActivatedRoute,
              private router: Router, private messageService: MessageService) { }

  ngOnInit(): void {
    if(this.route.snapshot.data['logout']){
      this.userService.logout();
      this.userService.sendUpdate();
      this.router.navigate(["/"]);
    }
    else this.login = this.route.snapshot.data['login'];
  }

  showPwd(){
    let node = document.getElementById("password");
    let i = node.parentNode.querySelector("i");

    if(node["type"] == "password"){
      node["type"] = "text";
      i.className = "pi pi-eye-slash";
    }
    else{
      node["type"] = "password";
      i.className = "pi pi-eye";
    }
  }

  loginFct(){
    if(this.username == null || this.username == ""
          || this.password == null || this.password == "") return;

    this.userService.login(this.username, this.password).subscribe(ok =>{
      this.userService.saveAuth();
      this.userService.sendUpdate();
      this.messageService.add({severity:'success', summary:'Connexion réussie', detail:'Vous êtes connecté !'});
      this.router.navigate(["/"]);
    }, error =>{
      console.log(error.status, error);
      this.messageService.add({severity:'error', summary:'Échec de la connexion', detail:"Mot de passe incorrect ou pseudo inconnu."});
    });
  }

  register(){
    if(this.username == null || this.username == ""
          || this.password == null || this.password == "") return;

    this.userService.register(this.username, this.password).subscribe(ok =>{
      this.userService.saveAuth();
      this.userService.sendUpdate();
      this.messageService.add({severity:'success', summary:'Inscription réussie', detail:'Vous êtes connecté !'});
      this.router.navigate(["/"]);
    }, error =>{
      console.log(error.status, error);
      this.messageService.add({severity:'error', summary:"Échec de l'inscription", detail:"Veuillez essayer un autre pseudo."});
    });
  }
}
