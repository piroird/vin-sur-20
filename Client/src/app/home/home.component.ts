import { Component, OnInit } from '@angular/core';
import { WineService } from "../wine.service";
import { Wine } from "../wine";
import { SelectItem } from 'primeng/api';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  wineList: any[] = [];
  search: string;
  showFilters: boolean = false;
  rangeValues=[1900,2021];
  selectedOrigin: SelectItem;
  origin: SelectItem[];
  
  constructor(private wineService:WineService) {
    this.origin = [{label: "Alsace", value: "Alsace"},
    {label: "Armagnac/Cognac", value: "Armagnac/Cognac"},
    {label: "Beaujolais", value: "Beaujolais"},
    {label: "Bordeaux", value: "Bordeaux"},
    {label: "Bourgogne", value: "Bourgogne"},
    {label: "Champagne", value: "Bourgogne"},
    {label: "Corse", value: "Bourgogne"},
    {label: "Jura", value: "Bourgogne"},
    {label: "Languedoc", value: "Bourgogne"},
    {label: "Lorraine", value: "Bourgogne"},
    {label: "Poitou-Charentes", value: "Bourgogne"},
    {label: "Provence", value: "Bourgogne"},
    {label: "Roussillon", value: "Bourgogne"},
    {label: "Savoie", value: "Bourgogne"},
    {label: "Sud-Ouest", value: "Bourgogne"},
    {label: "Vallée de la Loire", value: "Bourgogne"},
    {label: "Vallée du Rhône", value: "Bourgogne"}]
  }

  ngOnInit(): void {
    this.wineService.getAllWines().subscribe(wines => this.wineList = wines);
  }

  wineDescription(wine: Wine): string{
    return `${wine.origin}, domaine ${wine.domain}, ${wine.year}`;
  }

  getLink(wine: Wine): string{
    return `/vin/${wine.id}`;
  }
}
