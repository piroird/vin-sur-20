import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment as env} from '../environments/environment';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Wine } from "./wine";
import { Recipe } from "./recipe";

@Injectable({
  providedIn: 'root'
})
export class WineService {
  constructor(private http: HttpClient) { }

  getAllWines(): Observable<Wine[]>{
    return this.http.get<Wine[]>(env.apiURL+"allWines");
  }

  getFavWines(): Observable<Wine[]>{
    return this.http.get<Wine[]>(env.apiURL+"getFavWine");
  }

  getWine(id: Number): Observable<Wine>{
    return this.http.get<Wine>(`${env.apiURL}getWine?id=${id}`);
  }

  getMatchedRecipe(color: String): Observable<Recipe[]>{
    return this.http.get<Recipe[]>(`${env.apiURL}recipesMatchedWine?color=${color}`);
  }

  newWine(data): Observable<boolean>{
    let headers = new HttpHeaders({'encrypt': 'multipart/form-data'});
    return this.http.post<boolean>(env.apiURL+"newWine", data, {headers: headers});
  }

  favWine(idV, value): Observable<boolean>{
    return this.http.post<boolean>(env.apiURL+"favWine", {idV: idV, value: value});
  }
}
