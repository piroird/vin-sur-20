import { Component, OnInit } from '@angular/core';
import { SelectItem, MessageService } from 'primeng/api';
import { RecipeService } from '../recipe.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-recipe',
  templateUrl: './add-recipe.component.html',
  styleUrls: ['./add-recipe.component.scss']
})
export class AddRecipeComponent implements OnInit {
  colors: any[];
  selectedColor: string[];
  file_img: File;
  name: string;
  time: string;
  description: string;

  constructor(private router: Router, private messageService: MessageService, private recipeService: RecipeService) { }

  ngOnInit(): void {
    this.colors = [
      {name: 'Blanc', code: 'Blanc'},
      {name: 'Rouge', code: 'Rouge'},
      {name: 'Rosé', code: 'Rosé'},
    ]
  }

  openInput(){
    document.getElementById("inputFile").click();
  }

  onFileChange(event){
    let f: File = event.target.files[0];
    if (f.size > 1000000){
      this.messageService.add({severity:'error', summary:'Fichier trop lourd', detail:"Impossible d'ajouter une image de +1Mo."});
      event.target.value = "";
      this.file_img = null;
    }
    else{
      this.file_img = f;
      let span = document.querySelector("div#upload span") as HTMLElement;
      span.innerText = f.name;
    }
}

  submitForm(){
    let data = new FormData();
    data.append("file", this.file_img);
    data.append("nom", this.name);
    data.append("temps", this.time);
    data.append("accords", this.selectedColor.join(","));
    data.append("preparation", this.description);

    this.recipeService.newRecipe(data).subscribe(() => {
      this.router.navigate(["/liste-recettes"]);
    });
  }
}
