export interface Recipe {
    id: number;
    name: string;
    time: string;
    preparation: string;
    img?: string;
}
