export interface Wine {
    id: number;
    title: string;
    color: string;
    bubble: boolean;
    grape_variety: string;
    origin: string;
    domain: string;
    year: number;
    description: string;
    img?: string;
    note?: number;
    fav?: boolean;
    descr: string;
}
