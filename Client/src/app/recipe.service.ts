import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment as env} from '../environments/environment';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Recipe } from "./recipe";
import { Wine} from "./wine";


@Injectable({
  providedIn: 'root'
})

export class RecipeService {

  constructor(private http: HttpClient) { }

  getAllRecipes(): Observable<Recipe[]>{
    return this.http.get<Recipe[]>(env.apiURL+"allRecipes");
  }

  getRecipe(id: Number): Observable<Recipe>{
    return this.http.get<Recipe>(`${env.apiURL}getRecipe?id=${id}`);
  }

  getMatchedWine(id: Number): Observable<Wine[]>{
    return this.http.get<Wine[]>(`${env.apiURL}winesMatchedRecipe?idR=${id}`);
  }

  newRecipe(data): Observable<boolean>{
    let headers = new HttpHeaders({'encrypt': 'multipart/form-data'});
    return this.http.post<boolean>(env.apiURL+"newRecipe", data, {headers: headers});
  }

}
